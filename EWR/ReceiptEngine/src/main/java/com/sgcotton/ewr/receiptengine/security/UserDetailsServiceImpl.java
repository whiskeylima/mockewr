package com.sgcotton.ewr.receiptengine.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.account.CollateralAgentAccount;
import com.sgcotton.ewr.receiptengine.domain.account.GrowerAccount;
import com.sgcotton.ewr.receiptengine.domain.account.MerchantAccount;
import com.sgcotton.ewr.receiptengine.domain.account.WarehouseAccount;
import com.sgcotton.ewr.receiptengine.service.AccountService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AccountService accountService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountService.getAccountById(username);
		User currentUserAccount = null;
		if (account != null) {
			if (account instanceof WarehouseAccount)
			{
				currentUserAccount = new User(account.getAccountId(), account.getPassword(), true, true, true, true,
						AuthorityUtils.createAuthorityList("ROLE_WAREHOUSE"));
			}
			else if (account instanceof MerchantAccount)
			{
				currentUserAccount = new User(account.getAccountId(), account.getPassword(), true, true, true, true,
						AuthorityUtils.createAuthorityList("ROLE_MERCHANT"));
			}
			else if (account instanceof GrowerAccount)
			{
				currentUserAccount = new User(account.getAccountId(), account.getPassword(), true, true, true, true,
						AuthorityUtils.createAuthorityList("ROLE_GROWER"));
			}
			else if (account instanceof CollateralAgentAccount)
			{
				currentUserAccount = new User(account.getAccountId(), account.getPassword(), true, true, true, true,
						AuthorityUtils.createAuthorityList("ROLE_COLLATERAL_AGENT"));
			}
			return currentUserAccount;
		}
		throw new UsernameNotFoundException("Invalid account id.");
	}
}
