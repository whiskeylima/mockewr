package com.sgcotton.ewr.receiptengine.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.sgcotton.ewr.receiptengine.controller.exception.ReceiptNotFoundForUserException;
import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.account.WarehouseAccount;
import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;
import com.sgcotton.ewr.receiptengine.domain.receipt.ElectronicWarehouseReceipt;
import com.sgcotton.ewr.receiptengine.rest.receipt.CottonReceipt;
import com.sgcotton.ewr.receiptengine.rest.receipt.EWRReceipt;
import com.sgcotton.ewr.receiptengine.service.AccountService;
import com.sgcotton.ewr.receiptengine.service.ReceiptService;

@RestController
@RequestMapping("/receipt")
public class ReceiptController {
	private static final String UNIQUE_CONSTRAINTS_DUPLICATE_ENTRY = "Duplicate entry";

	@Autowired
	private AccountService accountService;

	@Autowired
	private ReceiptService receiptService;

	private CottonReceipt newReceipt;

	@RequestMapping(value="/", method = RequestMethod.GET)
	public HttpEntity<ResourceSupport> showUserReceipts(Principal user) {
		Account currentAccount = accountService.getAccountById(user.getName());
		ResourceSupport resourceSupport = new ResourceSupport();
		if (currentAccount.getClass().isInstance(WarehouseAccount.class)) {
			System.out.println("im a warehouse");
		} 
		else {
			List<ElectronicWarehouseReceipt> receipts = receiptService.findReceiptsByOwner(currentAccount);
			for (ElectronicWarehouseReceipt receipt : receipts) {
				resourceSupport.add(linkTo(
						methodOn(ReceiptController.class).showReceipt(user, receipt.getId().toString())).withRel(
						"Receipts"));
			}
		}

		return new ResponseEntity<ResourceSupport>(resourceSupport, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('ROLE_WAREHOUSE')")
	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public void createReceipt(Principal user, @RequestBody CottonReceipt newReceipt) {
		this.newReceipt = newReceipt;
		WarehouseAccount currentAccount = (WarehouseAccount) accountService.getAccountById(user.getName());
		Account currentBearer = accountService.getAccountById(newReceipt.getCurrentBearer());
		ElectronicWarehouseReceipt newEWR = new ElectronicWarehouseReceipt(newReceipt, currentAccount, currentBearer);
		receiptService.createReceipt(newEWR);
	}

	@RequestMapping(value = "/{receiptId}", method = RequestMethod.GET)
	public EWRReceipt showReceipt(Principal user, @PathVariable String receiptId) {
		Account currentBearer = accountService.getAccountById(newReceipt.getCurrentBearer());
		ElectronicWarehouseReceipt electronicWarehouseReceipt = receiptService.findReceiptsByIdAndCurrentBearer(
				receiptId, currentBearer);
		if (electronicWarehouseReceipt == null) {
			throw new ReceiptNotFoundForUserException();
		}
		return new CottonReceipt(electronicWarehouseReceipt);
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	// @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason =
	// "Fail to create receipt.")
	public void receiptCreationFailed(DataIntegrityViolationException ex, HttpServletResponse response)
			throws IOException {
		if ((ex.getMostSpecificCause() instanceof MySQLIntegrityConstraintViolationException)
				&& ((MySQLIntegrityConstraintViolationException) ex.getMostSpecificCause()).getMessage().contains(
						UNIQUE_CONSTRAINTS_DUPLICATE_ENTRY)) {
			String nonUniqueBaleNumbers = findExistingBales(newReceipt.getCottonData());
			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
					"The following bale ID(s) are part of another EWR(s): [" + nonUniqueBaleNumbers + "]");
		}
	}

	@ExceptionHandler(value = ReceiptNotFoundForUserException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Fail to find receipt for user.")
	public void findReceiptFailed(ReceiptNotFoundForUserException ex) {

	}

	private String findExistingBales(List<CommodityData> cottonDataList) {
		return receiptService.findDuplicateBales(cottonDataList);
	}
}
