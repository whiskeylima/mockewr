package com.sgcotton.ewr.receiptengine.domain.receipt;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import lombok.Data;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.account.WarehouseAccount;
import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;
import com.sgcotton.ewr.receiptengine.rest.receipt.CottonReceipt;

@Entity
@Data
public class ElectronicWarehouseReceipt {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name="uuid", strategy = "uuid2")
	@Column(columnDefinition = "varbinary(36)", length = 36)
	private UUID id;
	//private String receiptId;
	
	@ManyToOne(optional=false)
	private WarehouseAccount receiptCreator;
	
	@Type(type="timestamp")
	@Column(nullable=false)
	private Timestamp timestampCreated;
	
	@OneToMany
	@Column(nullable=false)
	private List <CommodityData> commodityData = new LinkedList<>();
	
	@ManyToOne(optional=false)
	private Account currentBearer;
	
	@ManyToOne
	private Account newBearer;
	
	@ElementCollection
	@Column(nullable=false)
	private List <ReceiptHistory> receiptHistory = new LinkedList<>();
	
	@Version
	private long version;
	
	public ElectronicWarehouseReceipt()
	{
		
	}
	public ElectronicWarehouseReceipt(CottonReceipt receipt, WarehouseAccount currentAccount, Account currentBearer)
	{
		commodityData = receipt.getCottonData();
		setReceiptCreator(currentAccount);
		setTimestampCreated(new Timestamp(System.currentTimeMillis()));
		setCurrentBearer(currentBearer);
		ReceiptHistory receiptHistory = new ReceiptHistory();
		receiptHistory.setModifiedByUser(currentAccount);
		receiptHistory.setReceiptState(ReceiptState.RECEIPT_CREATED);
		getReceiptHistory().add(receiptHistory);
	}
}
