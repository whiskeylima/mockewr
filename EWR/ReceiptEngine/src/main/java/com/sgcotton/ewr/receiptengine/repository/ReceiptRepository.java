package com.sgcotton.ewr.receiptengine.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;
import com.sgcotton.ewr.receiptengine.domain.receipt.ElectronicWarehouseReceipt;

@Repository
public interface ReceiptRepository extends JpaRepository <ElectronicWarehouseReceipt, UUID>{
	public ElectronicWarehouseReceipt findByCommodityData(CommodityData commodityDara);
	public List <ElectronicWarehouseReceipt> findAllByCurrentBearer(Account account);
	public ElectronicWarehouseReceipt findByIdAndCurrentBearer(UUID id, Account currentBearer);
}
