package com.sgcotton.ewr.receiptengine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.account.CollateralAgentAccount;
import com.sgcotton.ewr.receiptengine.domain.account.GrowerAccount;
import com.sgcotton.ewr.receiptengine.domain.account.MerchantAccount;
import com.sgcotton.ewr.receiptengine.domain.account.WarehouseAccount;
import com.sgcotton.ewr.receiptengine.repository.ReceiptRepository;
import com.sgcotton.ewr.receiptengine.service.AccountService;

@SpringBootApplication
public class ReceiptEngine implements CommandLineRunner {
	@Autowired
	private AccountService accountService;	
	
	@Autowired
	private ReceiptRepository receiptRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(ReceiptEngine.class, args);
	}

	public void run(String... args) throws Exception {
		createGrowerAccs();
		createMerchantAccs();
		createWarehouseAccs();
		createBankAccs();
	}
	
	private void createGrowerAccs()
	{
		Account account = new GrowerAccount();
		account.setFirstName("Winston");
		account.setLastName("Lee");
		account.setPassword(bCryptPasswordEncoder.encode("winston"));
		account.setAccountId("WINSTON_GROWER");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new GrowerAccount();
		account.setFirstName("Roy");
		account.setLastName("Chan");
		account.setPassword(bCryptPasswordEncoder.encode("roy"));
		account.setAccountId("ROY_GROWER");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new GrowerAccount();
		account.setFirstName("Tao");
		account.setLastName("Mai");
		account.setPassword(bCryptPasswordEncoder.encode("tao"));
		account.setAccountId("TAO_GROWER");
		account.setCompanyName("Company");
		accountService.createAccount(account);
	}
	
	private void createMerchantAccs()
	{
		Account account = new MerchantAccount();
		account.setFirstName("Winston");
		account.setLastName("Lee");
		account.setPassword(bCryptPasswordEncoder.encode("winston"));
		account.setAccountId("WINSTON_MERCHANT");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new MerchantAccount();
		account.setFirstName("Roy");
		account.setLastName("Chan");
		account.setPassword(bCryptPasswordEncoder.encode("roy"));
		account.setAccountId("ROY_MERCHANT");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new MerchantAccount();
		account.setFirstName("Tao");
		account.setLastName("Mai");
		account.setPassword(bCryptPasswordEncoder.encode("tao"));
		account.setAccountId("TAO_MERCHANT");
		account.setCompanyName("Company");
		accountService.createAccount(account);
	}
	
	private void createWarehouseAccs()
	{
		Account account = new WarehouseAccount();
		account.setFirstName("Winston");
		account.setLastName("Lee");
		account.setPassword(bCryptPasswordEncoder.encode("winston"));
		account.setAccountId("WINSTON_WAREHOUSE");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new WarehouseAccount();
		account.setFirstName("Roy");
		account.setLastName("Chan");
		account.setPassword(bCryptPasswordEncoder.encode("roy"));
		account.setAccountId("ROY_WAREHOUSE");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new WarehouseAccount();
		account.setFirstName("Tao");
		account.setLastName("Mai");
		account.setPassword(bCryptPasswordEncoder.encode("tao"));
		account.setAccountId("TAO_WAREHOUSE");
		account.setCompanyName("Company");
		accountService.createAccount(account);
	}
	
	private void createBankAccs()
	{
		Account account = new CollateralAgentAccount();
		account.setFirstName("Winston");
		account.setLastName("Lee");
		account.setPassword(bCryptPasswordEncoder.encode("winston"));
		account.setAccountId("WINSTON_BANK");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new CollateralAgentAccount();
		account.setFirstName("Roy");
		account.setLastName("Chan");
		account.setPassword(bCryptPasswordEncoder.encode("roy"));
		account.setAccountId("ROY_BANK");
		account.setCompanyName("Company");
		accountService.createAccount(account);
		
		account = new CollateralAgentAccount();
		account.setFirstName("Tao");
		account.setLastName("Mai");
		account.setPassword(bCryptPasswordEncoder.encode("tao"));
		account.setAccountId("TAO_BANK");
		account.setCompanyName("Company");
		accountService.createAccount(account);
	}
}
