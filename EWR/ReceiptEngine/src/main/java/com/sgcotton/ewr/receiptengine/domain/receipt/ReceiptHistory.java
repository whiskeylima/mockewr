package com.sgcotton.ewr.receiptengine.domain.receipt;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

import lombok.Data;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
@Data
@Embeddable
public class ReceiptHistory {
	private ReceiptState receiptState;
	
	@OneToOne
	private Account modifiedByUser;
}
