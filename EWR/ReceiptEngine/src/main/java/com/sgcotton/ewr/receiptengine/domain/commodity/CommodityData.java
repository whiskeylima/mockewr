package com.sgcotton.ewr.receiptengine.domain.commodity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
@Inheritance
@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class CommodityData {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name="uuid", strategy = "uuid2")
	@Column(columnDefinition = "varbinary(36)", length = 36)
	private UUID id;
}
