package com.sgcotton.ewr.receiptengine.domain.account;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class CollateralAgentAccount extends Account{

}
