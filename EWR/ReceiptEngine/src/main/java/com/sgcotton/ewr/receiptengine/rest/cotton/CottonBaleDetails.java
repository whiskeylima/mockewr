package com.sgcotton.ewr.receiptengine.rest.cotton;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@EqualsAndHashCode(callSuper=false)
public class CottonBaleDetails extends ResourceSupport {
	private final String baleNumber;

    @JsonCreator
    public CottonBaleDetails(@JsonProperty("baleNumber") String baleNumber) {
        this.baleNumber = baleNumber;
    }
}
