package com.sgcotton.ewr.receiptengine.rest.receipt;

import java.util.LinkedList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;
import com.sgcotton.ewr.receiptengine.domain.commodity.cotton.CottonData;
import com.sgcotton.ewr.receiptengine.domain.receipt.ElectronicWarehouseReceipt;

@Data
@EqualsAndHashCode(callSuper = false)
public class CottonReceipt extends EWRReceipt {	
	private List <CottonData> cottonDataList = new LinkedList<>();
	
	private String currentBearer;
	
	public CottonReceipt()
	{
		super("");
	}
	
	public CottonReceipt(String receiptId)
	{
		super(receiptId);
	}
	
	public CottonReceipt(ElectronicWarehouseReceipt electronicWarehouseReceipt) {
		super (electronicWarehouseReceipt.getId().toString());
		currentBearer = electronicWarehouseReceipt.getCurrentBearer().getAccountId();
		for (CommodityData commodityData:electronicWarehouseReceipt.getCommodityData())
		{	
			cottonDataList.add((CottonData)commodityData);
		}
	}
	public List<CommodityData> getCottonData()
	{
		List<CommodityData> commodityData = new LinkedList<>();
		for (CottonData cottonData : cottonDataList)
		{
			commodityData.add(cottonData);
		}
		return commodityData;
	}
}
