package com.sgcotton.ewr.receiptengine.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;
import com.sgcotton.ewr.receiptengine.domain.commodity.cotton.CottonData;
import com.sgcotton.ewr.receiptengine.domain.receipt.ElectronicWarehouseReceipt;
import com.sgcotton.ewr.receiptengine.rest.cotton.CottonBales;
import com.sgcotton.ewr.receiptengine.rest.cotton.CottonBaleDetails;
import com.sgcotton.ewr.receiptengine.service.AccountService;
import com.sgcotton.ewr.receiptengine.service.ReceiptService;

@RestController
@RequestMapping("/bale")
public class BaleController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ReceiptService receiptService;

	@RequestMapping(method = RequestMethod.GET)
	@PreAuthorize("hasRole('ROLE_WAREHOUSE') || hasRole('ROLE_GROWER')")
	public HttpEntity<CottonBales> showBales(Principal principal) {
		Account account = accountService.getAccountById(principal.getName());
		List<ElectronicWarehouseReceipt> receipts = receiptService.findReceiptsByOwner(account);
		CottonBales cottonBale = new CottonBales("Cotton");
		cottonBale.add(linkTo(methodOn(BaleController.class).showBales(principal)).withSelfRel());
		for (ElectronicWarehouseReceipt receipt: receipts)
		{
			List<CommodityData> commodityDataList = receipt.getCommodityData();
			for (CommodityData commodityData: commodityDataList)
			{
				List<String> baleNoList = ((CottonData)commodityData).getBaleNo();
				for (String baleNo: baleNoList)
				{
					cottonBale.add(linkTo(methodOn(BaleController.class).showBaleDetails(baleNo)).withRel(baleNo));
				}
			}
		}
		
        return new ResponseEntity<CottonBales>(cottonBale, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{baleid}")
	@PreAuthorize("hasRole('ROLE_WAREHOUSE')")
	public HttpEntity<CottonBaleDetails> showBaleDetails(@PathVariable String baleid) {
		return null;
	}
}
