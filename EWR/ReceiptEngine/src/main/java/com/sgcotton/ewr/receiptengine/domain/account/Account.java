package com.sgcotton.ewr.receiptengine.domain.account;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

import lombok.Data;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Inheritance
@Data
public abstract class Account {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name="uuid", strategy = "uuid2")
	@Column(columnDefinition = "varbinary(36)", length = 36)
	private UUID id;
	
	@Column(unique = true, nullable = false)
	private String accountId;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;
	
	@Column(nullable = false)
	private String companyName;
}
