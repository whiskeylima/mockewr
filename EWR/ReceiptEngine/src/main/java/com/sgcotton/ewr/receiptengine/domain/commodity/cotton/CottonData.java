package com.sgcotton.ewr.receiptengine.domain.commodity.cotton;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class CottonData extends CommodityData {
	@ElementCollection
	@Column(unique=true)
	private List <String> baleNo = new LinkedList<>();

	@Column(nullable=false)
	private String classingQuality;
}
