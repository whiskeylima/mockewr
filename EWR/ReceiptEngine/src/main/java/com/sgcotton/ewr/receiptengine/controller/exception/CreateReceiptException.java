package com.sgcotton.ewr.receiptengine.controller.exception;

public class CreateReceiptException extends RuntimeException {

	public CreateReceiptException(String message) {
		super(message);
	}
}
