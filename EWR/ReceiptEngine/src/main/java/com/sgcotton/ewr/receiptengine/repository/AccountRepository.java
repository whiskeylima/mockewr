package com.sgcotton.ewr.receiptengine.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgcotton.ewr.receiptengine.domain.account.Account;

@Repository
public interface AccountRepository extends JpaRepository <Account, UUID>{
	public Account findByAccountId(String accountId);
}
