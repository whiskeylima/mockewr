package com.sgcotton.ewr.receiptengine.domain.account;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class WarehouseAccount extends Account{
	private String warehouseAddress;
}
