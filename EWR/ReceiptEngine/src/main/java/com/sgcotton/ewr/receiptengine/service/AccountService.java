package com.sgcotton.ewr.receiptengine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Transactional
	public void createAccount(Account newAccount) {
		accountRepository.save(newAccount);
	}

	public Account getAccountById(String id) {
		return accountRepository.findByAccountId(id);
	}	

	public void updateAccount(Account account) {
		accountRepository.save(account);
	}	
}
