package com.sgcotton.ewr.receiptengine.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
	//@RequestMapping("warehouse")
	@RequestMapping("/")
		
	public void getWarehouseIndex()
	{
		System.out.println("You're a warehouse.");
	}
	
	//@RequestMapping("grower")
	@PreAuthorize("hasRole('ROLE_GROWER')")	
	public void getGrowerIndex()
	{
		System.out.println("You're a grower.");
	}
	
	//@RequestMapping("merchant")
	@PreAuthorize("hasRole('ROLE_MERCHANT')")	
	public void getMerchantIndex()
	{
		System.out.println("You're a merchant.");
	}
	
	//@RequestMapping("collateralagent")
	@PreAuthorize("hasRole('ROLE_COLLATERAL_AGENT')")	
	public void getCollateralAgentIndex()
	{
		System.out.println("You're a collateral agent.");
	}
}
