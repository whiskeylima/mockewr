package com.sgcotton.ewr.receiptengine.rest.receipt;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class EWRReceipt extends ResourceSupport {
	private final String receiptId;

	@JsonCreator
	public EWRReceipt(@JsonProperty("receiptId") String receiptId) {
		this.receiptId = receiptId;
	}
}
