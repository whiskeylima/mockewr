package com.sgcotton.ewr.receiptengine.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;

@Repository
public interface CommodityDataRepository extends JpaRepository <CommodityData, UUID>{
}
