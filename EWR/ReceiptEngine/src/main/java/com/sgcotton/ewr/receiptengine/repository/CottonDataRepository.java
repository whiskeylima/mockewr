package com.sgcotton.ewr.receiptengine.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sgcotton.ewr.receiptengine.domain.commodity.cotton.CottonData;

@Repository
public interface CottonDataRepository extends JpaRepository <CottonData, UUID>{
	/**
	 * Finds if a bale number exists. 
	 */
	public CottonData findByBaleNo(String baleNo);
}
