package com.sgcotton.ewr.receiptengine.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sgcotton.ewr.receiptengine.domain.account.Account;
import com.sgcotton.ewr.receiptengine.domain.commodity.CommodityData;
import com.sgcotton.ewr.receiptengine.domain.commodity.cotton.CottonData;
import com.sgcotton.ewr.receiptengine.domain.receipt.ElectronicWarehouseReceipt;
import com.sgcotton.ewr.receiptengine.repository.CommodityDataRepository;
import com.sgcotton.ewr.receiptengine.repository.CottonDataRepository;
import com.sgcotton.ewr.receiptengine.repository.ReceiptRepository;

@Service
public class ReceiptService {

	@Autowired
	private ReceiptRepository receiptRepository;

	@Autowired
	private CommodityDataRepository commodityDataRepository;
	
	@Autowired
	private CottonDataRepository cottonDataRepository;

	@Transactional
	public void createReceipt(ElectronicWarehouseReceipt receipt) {
		for (CommodityData commodityData : receipt.getCommodityData()) {
			commodityDataRepository.save(commodityData);
		}
		receiptRepository.save(receipt);
	}
	
	public ElectronicWarehouseReceipt findReceiptWithBaleNo(String baleNo)
	{
		CottonData cottonData = cottonDataRepository.findByBaleNo(baleNo);
		if (cottonData == null)
			return null;
		return receiptRepository.findByCommodityData(cottonData);
	}
	
	public String findDuplicateBales(List<CommodityData> cottonDataList)
	{
		StringBuilder duplicateBaleIds = new StringBuilder();
		for (CommodityData cottonData : cottonDataList) {
			List<String> baleNoList = ((CottonData) cottonData).getBaleNo();
			for (String baleNo : baleNoList) {
				CottonData data = cottonDataRepository.findByBaleNo(baleNo);
				if (data != null) {
					duplicateBaleIds.append(baleNo).append(" ");
				}
			}
		}
		return duplicateBaleIds.toString().trim();
	}
	
	public List <ElectronicWarehouseReceipt> findReceiptsByOwner(Account owner)
	{
		return receiptRepository.findAllByCurrentBearer(owner);
	}
	
	public ElectronicWarehouseReceipt findReceiptsByIdAndCurrentBearer(String id, Account currentBearer)
	{
		UUID uuid = UUID.fromString(id);
		return receiptRepository.findByIdAndCurrentBearer(uuid, currentBearer);
	}
}