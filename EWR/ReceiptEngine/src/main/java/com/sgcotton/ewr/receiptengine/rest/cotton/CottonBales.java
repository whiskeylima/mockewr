package com.sgcotton.ewr.receiptengine.rest.cotton;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@EqualsAndHashCode(callSuper=false)
public class CottonBales extends ResourceSupport {
	private final String baleType;

    @JsonCreator
    public CottonBales(@JsonProperty("baleType") String baleType) {
        this.baleType = baleType;
    }
}
